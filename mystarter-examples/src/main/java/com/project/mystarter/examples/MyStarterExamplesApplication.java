package com.project.mystarter.examples;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MyStarterExamplesApplication {

	public static void main(String[] args) {
		SpringApplication.run(MyStarterExamplesApplication.class, args);
	}
}
