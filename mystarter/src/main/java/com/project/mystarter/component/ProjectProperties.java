package com.project.mystarter.component;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Component
@ConfigurationProperties("com.project")
public class ProjectProperties {
	private String name;
	private String text;
}
