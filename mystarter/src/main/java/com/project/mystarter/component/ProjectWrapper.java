package com.project.mystarter.component;

import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Component
public class ProjectWrapper {
	private String name;
	private String text;

	public void setProperties(ProjectProperties projectProperties) {
		if (projectProperties.getName() != null) {
			this.setName(projectProperties.getName());
		} else {
			this.setName("default name");
		}
		if (projectProperties.getText() != null) {
			this.setText(projectProperties.getText());
		} else {
			this.setText("default text");
		}
	}
	
	@Override
	public String toString() {
		return "{\"name\":\"" + name + "\", \"text\":\"" + text + "\"}";
	}
}
