package com.project.mystarter.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.project.mystarter.component.ProjectProperties;
import com.project.mystarter.component.ProjectWrapper;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Configuration
@ConditionalOnProperty(name = "switch", havingValue = "on")
@EnableConfigurationProperties({ ProjectProperties.class })
public class ProjectAutoConfiguration {

	@Autowired
	private ProjectProperties projectProperties;

	@Bean
	@ConditionalOnClass({ ProjectWrapper.class })
	@ConditionalOnMissingBean({ ProjectWrapper.class })
	public ProjectWrapper initStarter() {
		ProjectWrapper pw = new ProjectWrapper();
		pw.setProperties(projectProperties);
		log.info("MyStarter initStarter init value is {}.", pw);
		return pw;
	}
}
